<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('password', function(){
	return Hash::make('qwerty');  
});
Route::get('/', 'AuthController@index')->name('index');
Auth::routes(['register' => false]);

Route::group(['middleware' => ['session']], function () {

	Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
	Route::get('metode', 'HomeController@metode')->name('metode');
	Route::post('fuzzy', 'HomeController@fuzzy')->name('fuzzy');

	Route::resource('users', 'UserController');
	Route::get('users/delete/{id}', 'UserController@delete');

	Route::resource('warung', 'WarungController');
	Route::get('warung/delete/{id}', 'WarungController@delete');

	Route::resource('meja', 'MejaController');
	Route::get('meja/delete/{id}', 'MejaController@delete');

	// Route::resource('menu', 'MenuController');
	Route::get('menu/delete/{id}/{warung_id}', 'MenuController@delete');
	Route::get('menu/{warung_id}/create', 'MenuController@create_menu');
	Route::get('menu/{id}', 'MenuController@show');
	Route::get('menu/{id}/edit', 'MenuController@edit');
	Route::post('menu/update/{id}', 'MenuController@update');
	Route::post('menu/store', 'MenuController@store');

	Route::resource('report', 'ReportController');

	Route::get('order', 'OrderController@index');
	Route::get('order/create', 'OrderController@create');
	Route::post('order/store', 'OrderController@post_create');
	Route::get('order/detail/{id}', 'OrderController@detail');
	Route::get('order/delete/{id}', 'OrderController@delete');
	Route::get('order/approve/{id}', 'OrderController@approve');


});
// Route::get('/home', 'HomeController@index')->name('home');
