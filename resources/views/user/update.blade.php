@extends('layouts.master')
@section('title','Update User')
@section('css')

@endsection
@section('content')
<section class="content-header">
  <h1>Master User</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{route('users.index')}}"> User</a></li>
    <li class="active">Update User</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update User</h3>
            </div>
            <div class="">
            	<form role="form" action="{{route('users.update',$user->id)}}" method="POST">
	            	{{csrf_field()}}
                @method('PUT')
	              	<div class="box-body">
                    <div class="form-group">
                      <label>Name</label>
                      <input type="text" required name="name" value="{{$user->name}}" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" required name="email" value="{{$user->email}}" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Username</label>
                      <input type="text" required name="username" value="{{$user->username}}" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label>New Password</label>
                      <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Type User</label>
                      <select name="type" required class="form-control" id="">
                        <option @if($user->type == 'warung') selected @endif value="warung">Admin Warung</option>
                        <option @if($user->type == 'pusat') selected @endif value="pusat">Admin Pusat</option>
                        <option @if($user->type == 'admin') selected @endif value="admin">SuperAdmin</option>
                      </select>
                    </div>
                  </div>
	              	<div class="box-footer">
		                <a href="{{route('users.index')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
		                <button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
	              	</div>
	            </form>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')

@endsection
