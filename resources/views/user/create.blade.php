@extends('layouts.master')
@section('title','Add New User')
@section('css')

@endsection
@section('content')
<section class="content-header">
  <h1>Master User</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{route('users.index')}}"> User</a></li>
    <li class="active">Add New User</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New User</h3>
            </div>
            <div class="">
            	<form role="form" action="{{route('users.store')}}" method="POST">
	            	{{csrf_field()}}
	              	<div class="box-body">
		                <div class="form-group">
		                  <label>Name</label>
		                  <input type="text" required name="name" required class="form-control">
		                </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input type="email" required name="email" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Username</label>
                      <input type="text" required name="username" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" required name="password" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Type User</label>
                      <select name="type" required class="form-control type" id="">
                        <option value="">-</option>
                        <option value="warung">Admin Warung</option>
                        {{-- <option value="pusat">Admin Pusat</option> --}}
                        <option value="admin">SuperAdmin</option>
                        <option value="kasir">Kasir</option>
                      </select>
                    </div>
                    <div class="form-group form-warung" style="display: none;">
                      <label>Pilih Warung</label>
                      <select name="warung_id" disabled class="form-control warung" style="display: none;">
                        @foreach($warung as $dat_warung)
                        <option value="{{$dat_warung->id}}">{{$dat_warung->nama}}</option>
                        @endforeach
                      </select>
                    </div>
	              	</div>
	              	<div class="box-footer">
		                <a href="{{route('users.index')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
		                <button class="btn btn-primary"><i class="fa fa-save"></i> Next</button>
	              	</div>
	            </form>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
  $('.type').change(function(){
    if($(this).val() == 'warung'){
      $('.form-warung').show();
      $('.warung').show();
      $('.warung').removeAttr('disabled');
    }else{
      $('.form-warung').hide();
      $('.warung').hide();
      $('.warung').attr('disabled',true);
    }
  });
</script>
@endsection
