@extends('layouts.master')
@section('title','Add New Warung')
@section('css')

@endsection
@section('content')
<section class="content-header">
  <h1>Master Warung</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{route('warung.index')}}"> Warung</a></li>
    <li class="active">Add New Warung</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Warung</h3>
            </div>
            <div class="">
            	<form role="form" action="{{route('warung.store')}}" method="POST">
	            	{{csrf_field()}}
	              	<div class="box-body">
		                <div class="form-group">
		                  <label>Nama Warung</label>
		                  <input type="text" required name="nama" required class="form-control">
		                </div>
	              	</div>
	              	<div class="box-footer">
		                <a href="{{route('warung.index')}}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
		                <button class="btn btn-primary"><i class="fa fa-save"></i> Next</button>
	              	</div>
	            </form>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')

@endsection
