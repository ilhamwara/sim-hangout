@extends('layouts.master')
@section('title','Add New Order')
@section('css')

@endsection
@section('content')
<section class="content-header">
  <h1>Master Menu</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Add New Order</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Order</h3>
            </div>
            <div class="">
            	<form role="form" action="{{url('order/store')}}" method="POST">
	            	{{csrf_field()}}
	              	<div class="box-body">
	              		<div class="form-group">
		                  <label>Nama Pesanan</label>
		                  <br>
		                  <span>
		                  	<a href="#" id="" class="text_pelanggan_baru">Nama Pelanggan Baru ?</a>
		                  	<a href="#" id="" style="display: none;" class="text_pelanggan_lama">Nama Pelanggan Lama ?</a>
		                  </span>
		                  <select name="nama" class="form-control select_nama" id="">
		                  	@foreach($order as $data_order)
		                  	<option value="{{$data_order->nama}}">{{$data_order->nama}}</option>
		                  	@endforeach
		                  </select>
		                  <input type="text" style="display: none;" name="nama"class="form-control text_pelanggan">
		                </div>
	              		<div class="form-group">
		                  <label>Meja</label>
		                  <select class="form-control" name="meja_id" required>
		                  	<option value="">-- Pilih Meja --</option>
		                  	@foreach($meja as $data_meja)
		                  	<option value="{{$data_meja->id}}">Meja {{$data_meja->nama}}</option>
		                  	@endforeach
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label>Menu</label>
		                  <select class="form-control menu" name="menu_id" required>
		                  	<option value="">-- Pilih Harga --</option>
		                  	@foreach($menu as $data_menu)
		                  	<option data-harga="{{$data_menu->harga}}" value="{{$data_menu->id}}">{{$data_menu->nama}}</option>
		                  	@endforeach
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label>Qty</label>
		                  <select class="form-control qty" name="qty" required>
		                  	<option value="">-- Pilih Qty --</option>
		                  	@for($i = 1; $i <= 10; $i++)
		                  	<option value="{{$i}}">{{$i}}</option>
		                  	@endfor
		                  </select>
		                </div>
		                <div class="form-group">
		                  <label>Harga</label>
		                  <input type="text" value="0" disabled required name="harga" required class="form-control harga">
		                </div>
	              	</div>
	              	<div class="box-footer">
		                <button class="btn btn-primary"><i class="fa fa-save"></i> Next</button>
	              	</div>
	            </form>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
	$('.menu').change(function(){
		var harga = $(this).find(':selected').data('harga');
		var qty = $('.qty').find(':selected').val();
		$('.harga').val(harga*qty);
	});
	$('.qty').change(function(){
		var harga = $('.menu').find(':selected').data('harga');
		var qty = $(this).find(':selected').val();
		$('.harga').val(harga*qty);
	});
	$('.text_pelanggan_baru').click(function(){
		$('.select_nama').hide();
		$('.select_nama').attr('disabled',true);

		$('.text_pelanggan_baru').hide();
		$('.text_pelanggan_lama').show();
		$('.text_pelanggan').show();
		$('.text_pelanggan').removeAttr('disabled');
	});
	$('.text_pelanggan_lama').click(function(){
		$('.text_pelanggan').hide();
		$('.text_pelanggan').attr('disabled',true);

		$('.text_pelanggan_baru').show();
		$('.text_pelanggan_lama').hide();
		$('.select_nama').show();
		$('.select_nama').removeAttr('disabled');
	});
</script>
@endsection
