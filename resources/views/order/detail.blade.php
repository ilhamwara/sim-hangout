@extends('layouts.master')
@section('title','Detail Order')
@section('css')
@endsection
@section('content')
<section class="content-header">
  <h1>Detail Order</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{url('report')}}"><i class="fa fa-dashboard"></i> Report</a></li>
    <li class="active">Detail Order</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box">
	            <div class="box-body">
	            	<h4>List Pesanan</h4>
	            	<table class="table table-bordered table-condensed">
	            		<tr>
	            			<td class="bg-primary text-center" colspan="3"><h4>Hangout Salihara</h4></td>
	            		</tr>
	            		<tr>
	            			<td width="15%">Nama</td>
	            			<td class="text-center" width="5%">:</td>
	            			<td>{{$order->nama}}</td>
	            		</tr>
	            		<tr>
	            			<td width="15%">No Meja</td>
	            			<td class="text-center" width="5%">:</td>
	            			<td>{{$order->meja->nama}}</td>
	            		</tr>
	            		<tr>
	            			<td width="15%">Status</td>
	            			<td class="text-center" width="5%">:</td>
	            			<td>
	            				@if($order->status == 0)
	            					<span class="label label-danger">Belum di bayar</span>
	            				@else
	            					<span class="label label-success">Sudah di bayar</span>
	            				@endif
	            			</td>
	            		</tr>
	            	</table>
	            	<br>
	            	<table class="table table-striped table-bordered table-condensed">
	            		<tr>
	            			<td colspan="4">Menu Pesanan</td>
	            		</tr>
	            		<tr>
	            			<td>No</td>
	            			<td>Menu</td>
	            			<td>Jumlah</td>
	            			<td>Harga</td>
	            		</tr>
	            		@foreach($pesanan as $k => $data_pesanan)
	            		<tr>
	            			<td width="5%">{{$k+1}}</td>
	            			<td width="35%">{{$data_pesanan->menu->nama}}</td>
	            			<td width="25%">{{$data_pesanan->qty}}</td>
	            			<td>Rp {{number_format($data_pesanan->harga)}}</td>
	            		</tr>
	            		@endforeach
	            		<tr>
	            			<td colspan="3">Total Bayar</td>
	            			<td>Rp {{number_format($pesanan->sum('harga'))}}</td>
	            		</tr>
	            	</table>
	            	@if(($order->status == 0) && (Auth::user()->type != 'warung'))
	            	<br>
	            	<a href="{{url('order/approve/'.$order->id)}}" onclick="return confirm('Anda yakin ingin membayar pesanan ini ?')" class="btn btn-success">Bayar Sekarang</a>
	            	@endif
	            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
	$(document).ready(function(){
		$("#order-table").on("click", ".remove_data", function () {
			var url = $(this).attr('data-href');
	        swal({
	            title: "Are You Sure ?",
	            text: "Delete this data cannot be restore",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "YES",
	            cancelButtonText: "NO",
	            closeOnConfirm: true,
	            closeOnCancel: true
	          },
	          function(isConfirm){
	            if (isConfirm) {
	              document.location.href = url;
	            }
	          });

	        return false;
		});
	});
</script>
@endsection
