@extends('layouts.master')
@section('title','Dashboard')
@section('css')
  	<!-- Morris chart -->
  	<link rel="stylesheet" href="{{asset('admin/bower_components/morris.js/morris.css')}}">
  	<!-- jvectormap -->
  	<link rel="stylesheet" href="{{asset('admin/bower_components/jvectormap/jquery-jvectormap.css')}}">
  	<!-- Date Picker -->
  	<link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  	<!-- Daterange picker -->
  	<link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  	<!-- bootstrap wysihtml5 - text editor -->
  	
@endsection
@section('content')
<section class="content-header">
  <h1> Dashboard <small>Control panel</small></h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{\App\Warung::count()}}</h3>
          <p>Warung</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          @if(Auth::user()->type == 'warung')
          <?php 
            $warung = \App\Menu::where('warung_id',Auth::user()->warung_id)->select('id')->get(); 
            $order = \App\Order::whereIn('menu_id',$warung)->count();
          ?>
          <h3>{{$order}}</h3>
          @else
          <h3>{{\App\Order::count()}}</h3>
          @endif

          <p>Order</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{\App\User::count()}}</h3>
          <p>Pengguna</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs pull-right">
          <li class="active"><a href="#chart" data-toggle="tab">Area</a></li>
          <li class="pull-left header"><i class="fa fa-inbox"></i> Penjual</li>
        </ul>
        <div class="tab-content no-padding">
          <!-- Morris chart - Sales -->
          <div class="chart tab-pane active" id="chart" style="position: relative; height: 300px;"></div>
          <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
        </div>
      </div>
    </div>
  	<div class="col-md-12">
      <h3>Hangout Salihara, 23 Oktober 2016</h3>
      <h4>Sebuah Pujasera ( Food Court ) yang dibangun di atas lahan sekitar -+ 400m2</h4>
  		<p>Hangout Salihara yang merupakan sebuah pujasera dengan model bangun dan tata letak ruang yang artistik,asri,sejuk dengan fasilitas yang memenuhi ac, free wifi, mushola, toilet yang bersih, bahkan penataan ruang yang cantik yang dapat untuk bersua foto bersama keluarga atau relasi.</p>
      <p>Menyediakan tempat acara untuk rapat, reuni, arisan, ulang tahun dan lain lain</p>
      <p>10 Booth/ warung dengan berbagai macam makanan tersedia. Beraneka ragam makanan tersedia di Food Court Hangout Salihara. Harga jual pun terjangkau Harga kaki 5 Rasa bintang 5. Dan di daerah yang sangat strategis terletak di jalan raya dan dekat dengan kampus, Perkantoran, bahkan santai dengan keluarga. </p>
      <p>Alamat nya berada di Hangout @Salihara, Jl. Salihara No.38-39, Pasar Minggu, Kec. Pasar Minggu, Jakarta Selatan, Jakarta</p>
  	</div>
    <div class="clearfix"></div>
    <br>
    <div class="col-md-4">
      <img src="{{asset('image.jpg')}}" alt="..." style="width: 90%" class="img-thumbnail">
    </div>
    <div class="col-md-4">
      <img src="{{asset('image1.jpg')}}" alt="..." style="width: 90%" class="img-thumbnail">
    </div>
    <div class="col-md-4">
      <img src="{{asset('image2.jpg')}}" alt="..." style="width: 90%" class="img-thumbnail">
    </div>
    <div class="col-md-4">
      <img src="{{asset('image3.jpg')}}" alt="..." style="width: 90%" class="img-thumbnail">
    </div>
    <div class="col-md-4">
      <img src="{{asset('image4.jpg')}}" alt="..." style="width: 90%" class="img-thumbnail">
    </div>
    <div class="col-md-4">
      <img src="{{asset('image5.png')}}" alt="..." style="width: 90%" class="img-thumbnail">
    </div>
    <div class="col-md-4">
      <img src="{{asset('image6.jpg')}}" alt="..." style="width: 90%" class="img-thumbnail">
    </div>
    <div class="col-md-4">
      <img src="{{asset('image7.jpg')}}" alt="..." style="width: 90%" class="img-thumbnail">
    </div>
  </div>
</section>
<!-- /.content -->
@endsection
@section('js')
	<!-- Morris.js charts -->
	<script src="{{asset('admin/bower_components/raphael/raphael.min.js')}}"></script>
	<script src="{{asset('admin/bower_components/morris.js/morris.min.js')}}"></script>
	<!-- Sparkline -->
	<script src="{{asset('admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
	<!-- jvectormap -->
	<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
	<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{asset('admin/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
	<!-- daterangepicker -->
	<script src="{{asset('admin/bower_components/moment/min/moment.min.js')}}"></script>
	<script src="{{asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	<!-- datepicker -->
	<script src="{{asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
  <script>
    Highcharts.chart('chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Penjualan Bulan {{date("M")}} Tahun {{date("Y")}}'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                '{{date("M")}}',
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [
        @php
          $order = \App\Order::with(['menu'])->whereMonth('tanggal',date('m'))->select('*', DB::raw('SUM(qty) as total'))->groupBy('menu_id')->orderBy('total','DESC')->limit(5)->get();
        @endphp
        @foreach($order as $data)
          {
              name: '{{$data->menu->nama}} - {{$data->menu->warung->nama}}',
              data: [
                {{( empty(\App\Order::where('menu_id',$data->menu_id)->select('*', DB::raw('SUM(qty) as total'))->value('total')) ? 0 : \App\Order::where('menu_id',$data->menu_id)->select('*', DB::raw('SUM(qty) as total'))->value('total') )}},
              ],

          },
        @endforeach
        ]
    });
  </script>
@endsection