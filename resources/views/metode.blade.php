@extends('layouts.master')
@section('title','Metode')
@section('css')
@endsection
@section('content')
<section class="content-header">
  <h1>Metode</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Metode</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box">
	            <div class="box-body">
	            	<div>
					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#fuzzy" aria-controls="fuzzy" role="tab" data-toggle="tab">Fuzzy Tahani</a></li>
					    <li role="presentation"><a href="#saw" aria-controls="saw" role="tab" data-toggle="tab">SAW</a></li>
					  </ul>

					  <!-- Tab panes -->
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane active" id="fuzzy">
					    	<br>
					    	<form class="forms-sample" action="{{url('fuzzy')}}" method="POST">
				                {{csrf_field()}}
				                <input type="hidden" name="type" value="fuzzy">
				                <div class="form-group">
				                    <label class="control-label">Harga</label>
				                    <select name="harga" class="form-control p-input">
				                        <option value="1">Mahal</option>
				                        <option value="50">Sedang</option>
				                        <option value="100">Murah</option>
				                    </select>
				                </div>
				                <div class="form-group">
				                    <label class="control-label">Kecepatan Penyajian</label>
				                    <select name="cepat_sajian" class="form-control p-input">
				                        <option value="1">Lama </option>
				                        <option value="50">Sedang</option>
				                        <option value="100">Cepat</option>
				                    </select>
				                </div>
				                <div class="form-group">
				                    <label class="control-label">Cara Penyajian</label>
				                    <select name="cara_sajian" class="form-control p-input">
				                        <option value="1">Tidak Menarik</option>
				                        <option value="50">Biasa</option>
				                        <option value="100">Menarik</option>
				                    </select>
				                </div>
				                <button type="submit" class="btn btn-primary">Cari</button>
				            </form>
				            @if(request()->type == 'fuzzy')
				            	@php

						           //harga
						          $min_harga    = 1;
						          $mid_harga    = 50;
						          $max_harga    = 100;
						          $nilai_harga1 = round(($mid_harga - request()->harga) / ($mid_harga - $min_harga));
						          $nilai_harga2 = round((request()->harga - $min_harga) / ($mid_harga - $min_harga));
						          $nilai_harga3 = round((request()->harga - $mid_harga) / ($max_harga - $mid_harga));

						          //cepat_sajian BARU
						          $min_cepat_sajian    = 1;
						          $mid_cepat_sajian    = 50;
						          $max_cepat_sajian    = 100;
						          $nilai_cepat_sajian1 = round(($mid_cepat_sajian - request()->cepat_sajian) / ($mid_cepat_sajian - $min_cepat_sajian));
						          $nilai_cepat_sajian2 = round((request()->cepat_sajian - $min_cepat_sajian) / ($mid_cepat_sajian - $min_cepat_sajian));
						          $nilai_cepat_sajian3 = round((request()->cepat_sajian - $mid_cepat_sajian) / ($max_cepat_sajian - $mid_cepat_sajian));

						          //cepat_sajian BARU
						          $min_cara_sajian    = 1;
						          $mid_cara_sajian    = 50;
						          $max_cara_sajian    = 100;
						          $nilai_cara_sajian1 = round(($mid_cara_sajian - request()->cara_sajian) / ($mid_cara_sajian - $min_cara_sajian));
						          $nilai_cara_sajian2 = round((request()->cara_sajian - $min_cara_sajian) / ($mid_cara_sajian - $min_cara_sajian));
						          $nilai_cara_sajian3 = round((request()->cara_sajian - $mid_cara_sajian) / ($max_cara_sajian - $mid_cara_sajian));

						          $cepat_sajian_terendah    = min($nilai_cepat_sajian1,$nilai_cepat_sajian2,$nilai_cepat_sajian3);
						          $cara_sajian_terendah     = min($nilai_cara_sajian1,$nilai_cara_sajian2,$nilai_cara_sajian3);
						          $harga_terendah     		= min($nilai_harga1,$nilai_harga2,$nilai_harga3);

						          $Z1 = (((request()->harga*$harga_terendah)+(request()->cepat_sajian*$cepat_sajian_terendah)+(request()->cara_sajian*$cara_sajian_terendah)));
						          $Z2 = (($harga_terendah+$cepat_sajian_terendah+$cara_sajian_terendah));
						          $nilai = ($Z1/$Z2);  
					            @endphp
					            <br>
					            <h4><b>Harga</b></h4>
					            <table class="table table-striped">
					                <tr>
					                    <td>Min Harga</td>
					                    <td>=</td>
					                    <td colspan="5"><b>1</b></td>
					                </tr>
					                <tr>
					                    <td>Mid Harga</td>
					                    <td>=</td>
					                    <td colspan="5"><b>{{($mid_harga)}}</b></td>
					                </tr>
					                <tr>
					                    <td>Max Harga</td>
					                    <td>=</td>
					                    <td colspan="5"><b>{{$max_harga}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Harga 1 </td> 
					                    <td>=</td>
					                    <td> (Mid Harga - Input Harga) / (Mid Harga - Min Harga)</td>
					                    <td> = </td> 
					                    <td>({{$mid_harga}} - {{request()->harga}}) / ({{$mid_harga}} - {{$min_harga}})</td>
					                    <td> = </td>
					                    <td><b>{{round(($mid_harga - request()->harga) / ($mid_harga - $min_harga))}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Harga 2 </td> 
					                    <td>=</td>
					                    <td>(Input Harga - Min Harga) / (Min Harga - Mid Harga)</td>
					                    <td> = </td> 
					                    <td>({{request()->harga}} - {{$min_harga}}) / ({{$mid_harga}} - {{$min_harga}})</td>
					                    <td> = </td>
					                    <td><b>{{round((request()->harga - $min_harga) / ($mid_harga - $min_harga))}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Harga 3 </td> 
					                    <td>=</td>
					                    <td>(Input Harga - Mid Harga) / (Max Harga - Mid Harga)</td>
					                    <td> = </td> 
					                    <td>({{request()->harga}} - {{$mid_harga}}) / ({{$max_harga}} - {{$mid_harga}})</td>
					                    <td> = </td>
					                    <td><b>{{round((request()->harga - $mid_harga) / ($max_harga - $mid_harga))}}</b></td>
					                </tr>
					            </table>

					            <h4><b>Kecepatan Penyajian</b></h4>
					            <table class="table table-striped">
					                <tr>
					                    <td>Min Kecepatan Penyajian</td>
					                    <td>=</td>
					                    <td colspan="5"><b>1</b></td>
					                </tr>
					                <tr>
					                    <td>Mid Kecepatan Penyajian</td>
					                    <td>=</td>
					                    <td colspan="5"><b>{{$mid_cepat_sajian}}</b></td>
					                </tr>
					                <tr>
					                    <td>Max Kecepatan Penyajian</td>
					                    <td>=</td>
					                    <td colspan="5"><b>{{$max_cepat_sajian}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Kecepatan Penyajian 1 </td> 
					                    <td>=</td>
					                    <td> (Mid Kecepatan Penyajian - Input Kecepatan Penyajian) / (Mid Kecepatan Penyajian - Min Kecepatan Penyajian)</td>
					                    <td> = </td> 
					                    <td>({{$mid_cepat_sajian}} - {{request()->cepat_sajian}}) / ({{$mid_cepat_sajian}} - {{$min_cepat_sajian}})</td>
					                    <td> = </td>
					                    <td><b>{{round(($mid_cepat_sajian - request()->cepat_sajian) / ($mid_cepat_sajian - $min_cepat_sajian))}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Kecepatan Penyajian 2 </td> 
					                    <td>=</td>
					                    <td>(Input Kecepatan Penyajian - Min Kecepatan Penyajian) / (Min Kecepatan Penyajian - Mid Kecepatan Penyajian)</td>
					                    <td> = </td> 
					                    <td>({{request()->cepat_sajian}} - {{$min_cepat_sajian}}) / ({{$mid_cepat_sajian}} - {{$min_cepat_sajian}})</td>
					                    <td> = </td>
					                    <td><b>{{round((request()->cepat_sajian - $min_cepat_sajian) / ($min_cepat_sajian - $mid_cepat_sajian))}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Kecepatan Penyajian 3 </td> 
					                    <td>=</td>
					                    <td>(Input Kecepatan Penyajian - Mid Kecepatan Penyajian) / (Max Kecepatan Penyajian - Mid Kecepatan Penyajian)</td>
					                    <td> = </td> 
					                    <td>({{request()->cepat_sajian}} - {{$mid_cepat_sajian}}) / ({{$max_cepat_sajian}} - {{$mid_cepat_sajian}})</td>
					                    <td> = </td>
					                    <td><b>{{round((request()->cepat_sajian - $mid_cepat_sajian) / ($max_cepat_sajian - $mid_cepat_sajian))}}</b></td>
					                </tr>
					            </table>

					            <h4><b>Cara Penyajian</b></h4>
					            <table class="table table-striped">
					                <tr>
					                    <td>Min Cara Sajian</td>
					                    <td>=</td>
					                    <td colspan="5"><b>1</b></td>
					                </tr>
					                <tr>
					                    <td>Mid Cara Sajian</td>
					                    <td>=</td>
					                    <td colspan="5"><b>{{$mid_cara_sajian}}</b></td>
					                </tr>
					                <tr>
					                    <td>Max Cara Sajian</td>
					                    <td>=</td>
					                    <td colspan="5"><b>{{$max_cara_sajian}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Cara Sajian 1 </td> 
					                    <td>=</td>
					                    <td> (Mid Cara Sajian - Input Cara Sajian) / (Mid Cara Sajian - Min Cara Sajian)</td>
					                    <td> = </td> 
					                    <td>({{$mid_cara_sajian}} - {{request()->cara_sajian}}) / ({{$mid_cara_sajian}} - {{$min_cara_sajian}})</td>
					                    <td> = </td>
					                    <td><b>{{$nilai_cara_sajian1}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Cara Sajian 2 </td> 
					                    <td>=</td>
					                    <td>(Input Cara Sajian - Min Cara Sajian) / (Min Cara Sajian - Mid Cara Sajian)</td>
					                    <td> = </td> 
					                    <td>({{request()->cara_sajian}} - {{$min_cara_sajian}}) / ({{$mid_cara_sajian}} - {{$min_cara_sajian}})</td>
					                    <td> = </td>
					                    <td><b>{{$nilai_cara_sajian2}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Cara Sajian 3 </td> 
					                    <td>=</td>
					                    <td>(Input Cara Sajian - Mid Cara Sajian) / (Max Cara Sajian - Mid Cara Sajian)</td>
					                    <td> = </td> 
					                    <td>({{request()->cara_sajian}} - {{$mid_cara_sajian}}) / ({{$max_cara_sajian}} - {{$mid_cara_sajian}})</td>
					                    <td> = </td>
					                    <td><b>{{$nilai_cara_sajian3}}</b></td>
					                </tr>
					            </table>                                   

					            <h4><b>Nilai Terendah</b></h4>
					            <table class="table table-striped">
					                <tr>
					                    <td>Nilai Harga </td>
					                    <td>=</td>
					                    <td>{{min($nilai_harga1,$nilai_harga2,$nilai_harga3)}}</td>
					                </tr>
					                <tr>
					                    <td>Nilai Cepat Penyajian </td>
					                    <td>=</td>
					                    <td>{{min($nilai_cepat_sajian1,$nilai_cepat_sajian2,$nilai_cepat_sajian3)}}</td>
					                </tr>
					                <tr>
					                    <td>Nilai Cara Penyajian </td>
					                    <td>=</td>
					                    <td>{{min($nilai_cara_sajian1,$nilai_cara_sajian2,$nilai_cara_sajian3)}}</td>
					                </tr>
					            </table>
					                
					            <h4><b>Hasil Akhir</b></h4>
					            <table class="table table-striped">
					                <tr>
					                    <td>Z1</td>
					                    <td>=</td>
					                    <td>Total Jumlah Input Bobot * default bobot terendah</td>
					                    <td>=</td>
					                    <td colspan="3"><b>{{(((request()->harga*$harga_terendah)+(request()->cepat_sajian*$cepat_sajian_terendah)+(request()->cara_sajian*$cara_sajian_terendah)))}}</b></td>
					                </tr>
					                <tr>
					                    <td>Z2</td>
					                    <td>=</td>
					                    <td>Total Jumlah default bobot terendah</td>
					                    <td>=</td>
					                    <td colspan="3"><b>{{(($harga_terendah+$cepat_sajian_terendah+$cara_sajian_terendah))}}</b></td>
					                </tr>
					                <tr>
					                    <td>Nilai Akhir</td>
					                    <td>=</td>
					                    <td>(Z1 / Z2) ( {{$Z1}} / {{$Z2}} )</td>
					                    <td>=</td>
					                    <td><b>{{$nilai}}</b></td>
					                    <td>=</td>
					                    <td>{{str_replace(',','.', number_format(round($nilai)))}}</td>
					                </tr>
					            </table>
					            <br><br>
					            <h4><b>Fuzzy Tahani Penjualan Terlaris</b></h4>
					          	<br>
					          	<h4><b>Penjualan</b></h4>
					            <table class="table table-bordered table-striped">
					            	<tr>
					            		<th>No</th>
					            		<th>Menu</th>
					            		<th>Warung</th>
					            		<th>Total Pembelian</th>
					            	</tr>
					            	@foreach($order as $k => $data_order)
					            	<tr>
					            		<th>{{$k+1}}</th>
					            		<th>{{$data_order->menu->nama}}</th>
					            		<th>{{$data_order->menu->warung->nama}}</th>
					            		<th>{{$data_order->total}}</th>
					            	</tr>
					            	@endforeach
					            </table>
					            <br>
					            <p>Dari hasil perhitungan maka makanan ini merupakan makanan yang di minati oleh pengunjung food court hangout Salihara</p>
					        @endif
					    </div>
					    <div role="tabpanel" class="tab-pane" id="saw">
					    	<h4><b>SAW Penjualan Terlaris</b></h4>
				          	<br>
				          	<form class="forms-sample" action="{{url('fuzzy')}}" method="POST">
				                {{csrf_field()}}
				                <input type="hidden" name="type" value="saw">
				                <div class="form-group">
				                    <label class="control-label">Harga</label>
				                    <select name="harga" class="form-control p-input">
				                        <option value="1">Mahal</option>
				                        <option value="50">Sedang</option>
				                        <option value="100">Murah</option>
				                    </select>
				                </div>
				                <div class="form-group">
				                    <label class="control-label">Kecepatan Penyajian</label>
				                    <select name="cepat_sajian" class="form-control p-input">
				                        <option value="1">Lama </option>
				                        <option value="50">Sedang</option>
				                        <option value="100">Cepat</option>
				                    </select>
				                </div>
				                <div class="form-group">
				                    <label class="control-label">Cara Penyajian</label>
				                    <select name="cara_sajian" class="form-control p-input">
				                        <option value="1">Tidak Menarik</option>
				                        <option value="50">Biasa</option>
				                        <option value="100">Menarik</option>
				                    </select>
				                </div>
				                <button type="submit" class="btn btn-primary">Cari</button>
				            </form>
				          	@if(request()->type == 'saw')
				            <br>
				            <h4>Bobot</h4>
				            <table class="table table-bordered table-striped">
				            	<tr>
				            		<th width="20%">Nama</th>
				            		<th>Nilai</th>
				            	</tr>
				            	<tr>
				            		<td>Harga</td>
				            		<td>60%</td>
				            	</tr>
				            	<tr>
				            		<td>Kecepatan Penyajian</td>
				            		<td>20%</td>
				            	</tr>
				            	<tr>
				            		<td>Cara Penyajian</td>
				            		<td>20%</td>
				            	</tr>
				            </table>
				            <!-- Normalisasi -->
					            <?php 
					            	$nor_1 = request()->harga/max(request()->harga,request()->cara_sajian,request()->cepat_sajian);
					            	$nor_2 = request()->cepat_sajian/max(request()->harga,request()->cara_sajian,request()->cepat_sajian);
					            	$nor_3 = request()->cara_sajian/max(request()->harga,request()->cara_sajian,request()->cepat_sajian);
					            	$bobot_1 = 0.6;
					            	$bobot_2 = 0.2;
					            	$bobot_3 = 0.2;
					            	$hasil = ($bobot_1*$nor_1)+($bobot_2*$nor_2)+($bobot_3*$nor_3);
					            ?>
					         <table class="table table-bordered table-striped">
					         	<tr>
					         		<th>Harga</th>
					         		<th>Kecepatan Penyajian</th>
					         		<th>Cara Penyajian</th>
					         	</tr>
					         	<tr>
					         		<td>{{request()->harga}}</td>
					         		<td>{{request()->cepat_sajian}}</td>
					         		<td>{{request()->cara_sajian}}</td>
					         	</tr>
					         </table>
					         <h4>Alternatif :</h4>
					         Harga = {{request()->harga}} / {{max(request()->harga,request()->cara_sajian,request()->cepat_sajian)}} = {{$nor_1}} <br>
					         Cepat Sajian = {{request()->cepat_sajian}} / {{max(request()->harga,request()->cara_sajian,request()->cepat_sajian)}} = {{$nor_2}} <br>
					         Cara Sajian = {{request()->cara_sajian}} / {{max(request()->harga,request()->cara_sajian,request()->cepat_sajian)}} = {{$nor_3}} <br>

					         <h4>Perhitungan dengan bobot : </h4>
					         Hasil = (Bobot Harga x Normalisasi Harga)+(Bobot Cepat Sajian x Normalisasi Cepat Sajian)+(Bobot Cara Sajian x Normalisasi Cara Sajian) <br>
					         Hasil = ({{$bobot_1}} x {{$nor_1}})+({{$bobot_2}} x {{$nor_2}})+({{$bobot_3}} x {{$nor_3}}) = {{$hasil}}<br>
					        <br>
					        <h4><b>Penjualan Terlaris Saat ini</b></h4>
				            <table class="table table-bordered table-striped">
				            	<tr>
				            		<th>No</th>
				            		<th>Menu</th>
				            		<th>Warung</th>
				            		<th>Total Pembelian</th>
				            	</tr>
				            	@foreach($order as $k => $data_order)
				            	<tr>
				            		<th>{{$k+1}}</th>
				            		<th>{{$data_order->menu->nama}}</th>
				            		<th>{{$data_order->menu->warung->nama}}</th>
				            		<th>{{$data_order->total}}</th>
				            	</tr>
				            	@endforeach
				            </table>
				            <p>dari hasil perhitungan maka makanan ini merupakan makanan yang di minati oleh pengunjung food court hangout Salihara</p>
				            @endif
					    </div>
					  </div>
					</div>
	            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')
@endsection
