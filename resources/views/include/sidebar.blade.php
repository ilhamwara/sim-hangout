<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li @if(Request::is('dashboard')) class="active" @endif>
        <a href="{{url('dashboard')}}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      @if((Auth::user()->type == 'admin') || (Auth::user()->type == 'warung'))
      <li @if(Request::is('order*')) class="active" @endif>
        <a href="{{url('order')}}">
          <i class="fa fa-calculator"></i> <span> Pesanan</span>
        </a>
      </li>
      @endif
      @if(Auth::user()->type == 'admin')
      <li @if(Request::is('warung*')) class="active" @endif>
        <a href="{{route('warung.index')}}">
          <i class="fa fa-home"></i> <span> Warung</span>
        </a>
      </li>
      <li @if(Request::is('meja*')) class="active" @endif>
        <a href="{{route('meja.index')}}">
          <i class="fa fa-table"></i> <span> Meja</span>
        </a>
      </li>
      <li @if(Request::is('metode*')) class="active" @endif>
        <a href="{{url('metode')}}">
          <i class="fa fa-line-chart"></i> <span> Metode</span>
        </a>
      </li>
      <li @if(Request::is('users*')) class="active" @endif>
        <a href="{{route('users.index')}}">
          <i class="fa fa-group"></i> <span> Management User</span>
        </a>
      </li>
      @endif
      <li @if(Request::is('report*')) class="active" @endif>
        <a href="{{route('report.index')}}">
          <i class="fa fa-database"></i> <span> Report</span>
        </a>
      </li>
      <li>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          <i class="fa fa-sign-out"></i> <span>{{ __('Logout') }}</span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
