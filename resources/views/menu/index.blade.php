@extends('layouts.master')
@section('title','Menu')
@section('css')
<link rel="stylesheet" href="{{asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/bower_components/datatables.net-bs/css/dataTables-button.css')}}">
@endsection
@section('content')
<section class="content-header">
  <h1>Data Menu {{$warung->nama}}</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{route('warung.index')}}">Data Warung</a></li>
    <li class="active">Data Menu {{$warung->nama}}</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box">
	            <div class="box-body">
	            	<div class="table-responsive">
	            		{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}
	            	</div>
	            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script src="{{asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/bower_components/datatables.net-bs/js/dataTables-button.js')}}"></script>
<script src="{{asset('admin/bower_components/datatables.net-bs/js/button-server.js')}}"></script>
{!! $dataTable->scripts() !!}
<script>
	$(document).ready(function(){
		$("#menu-table").on("click", ".remove_data", function () {
			var url = $(this).attr('data-href');
	        swal({
	            title: "Are You Sure ?",
	            text: "Delete this data cannot be restore",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "YES",
	            cancelButtonText: "NO",
	            closeOnConfirm: true,
	            closeOnCancel: true
	          },
	          function(isConfirm){
	            if (isConfirm) {
	              document.location.href = url;
	            }
	          });

	        return false;
		});
	});
</script>
@endsection
