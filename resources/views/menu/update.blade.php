@extends('layouts.master')
@section('title','Update Menu')
@section('css')

@endsection
@section('content')
<section class="content-header">
  <h1>Master Menu</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{url('menu/'.$menu->warung_id)}}"> Menu</a></li>
    <li class="active">Update Menu</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Update Menu</h3>
            </div>
            <div class="">
            	<form role="form" action="{{url('menu/update/'.$menu->id)}}" method="POST" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              	<div class="box-body">
                    <div class="form-group">
                      <label>Nama Menu</label>
                      <input type="text" required name="nama" value="{{$menu->nama}}" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Harga Menu</label>
                      <input type="text" required name="harga" value="{{$menu->harga}}" required class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Gambar</label>
                      <input type="file" name="gambar" class="form-control">
                    </div>
                  </div>
	              	<div class="box-footer">
		                <a href="{{url('menu/'.$menu->warung_id)}}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
		                <button class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
	              	</div>
	            </form>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')

@endsection
