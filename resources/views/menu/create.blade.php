@extends('layouts.master')
@section('title','Add New Menu')
@section('css')

@endsection
@section('content')
<section class="content-header">
  <h1>Master Menu</h1>
  <ol class="breadcrumb">
    <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="{{url('menu/'.$warung->id)}}"> Menu</a></li>
    <li class="active">Add New Menu</li>
  </ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			@include('include.alert')
			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Menu</h3>
            </div>
            <div class="">
            	<form role="form" action="{{url('menu/store')}}" method="POST" enctype="multipart/form-data">
	            	{{csrf_field()}}
	              	<div class="box-body">
	              		<input type="hidden" value="{{$warung->id}}" name="warung_id">
		                <div class="form-group">
		                  <label>Nama Menu</label>
		                  <input type="text" required name="nama" required class="form-control">
		                </div>
		                <div class="form-group">
		                  <label>Harga</label>
		                  <input type="text" required name="harga" required class="form-control">
		                </div>
		                <div class="form-group">
		                  <label>Gambar</label>
		                  <input type="file" required name="gambar" class="form-control">
		                </div>
	              	</div>
	              	<div class="box-footer">
		                <a href="{{url('menu/'.$warung->id)}}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
		                <button class="btn btn-primary"><i class="fa fa-save"></i> Next</button>
	              	</div>
	            </form>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection
@section('js')

@endsection
