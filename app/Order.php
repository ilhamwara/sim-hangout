<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $fillable = [
    	'nama',
    	'meja_id',
    	'menu_id',
    	'status',
    	'harga',
    	'qty',
    ];

    public function menu()
    {
    	return $this->belongsTo('\App\Menu','menu_id','id');
    }
    public function meja()
    {
    	return $this->belongsTo('\App\Meja','meja_id','id');
    }
}
