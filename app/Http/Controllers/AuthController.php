<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    public function index()
    {
    	if (Auth::check()) {
    		return redirect('dashboard');
    	}
    	return view('index');
    }
}
