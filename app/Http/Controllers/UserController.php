<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\UserDataTable;
use App\User;
use App\Warung;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserDataTable $dataTable)
    {
        return $dataTable->render('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warung = Warung::all();
        return view('user.create',compact('warung'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (User::where('username',$request->username)->count() > 0) {
            return redirect()->back()->with('alert-warning','Username sudah digunakan silahkan coba yang lain');
        }

        if (User::where('warung_id',$request->warung_id)->count() > 0) {
            return redirect()->back()->with('alert-warning','Data tidak boleh sama');
        }

        $user = new User;
        if (!empty($request->warung_id)) {
            $user->warung_id    = $request->warung_id;
        }
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->username     = $request->username;
        $user->password     = Hash::make($request->password);
        $user->type         = $request->type;
        $user->save();

        return redirect('users')->with('alert-success','Berhasil update data');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.update',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (User::where('id','!=',$id)->where('username',$request->username)->count() > 0) {
            return redirect()->back()->with('alert-warning','Username sudah digunakan silahkan coba yang lain');
        }

        $user = User::findOrFail($id);
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->username     = $request->username;
        if (!empty($request->password)) {
            $user->password     = Hash::make($request->password);
        }
        $user->type         = $request->type;
        $user->save();

        return redirect('users')->with('alert-success','Berhasil update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id)
    {
        User::findOrFail($id)->delete();
        return redirect('users')->with('alert-success','Success deleted data');
    }
}
