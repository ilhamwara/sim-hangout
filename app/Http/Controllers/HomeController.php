<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        return view('dashboard');
    }
    public function metode()
    {
        return view('metode');
    }
    public function fuzzy(Request $request)
    {

        // $order = Order::with(['menu'])->select('*', DB::raw('count(*) as total'))->groupBy('menu_id')->orderBy('total','DESC')->limit(5)->get();
        $order = Order::with(['menu'])->select('*', DB::raw('SUM(qty) as total'))->groupBy('menu_id')->orderBy('total','DESC')->limit(5)->get();

        if (request()->metode == 'fuzzy') {

            //HARGA BARU
            $min_harga = 1;
            $mid_harga = 50;
            $max_harga = 100;
            $nilai_harga1 = round(($mid_harga - $request->harga) / ($mid_harga - $min_harga));
            $nilai_harga2 = round(($request->harga - $min_harga) / ($mid_harga - $min_harga));
            $nilai_harga3 = round(($request->harga - $mid_harga) / ($max_harga - $mid_harga));

            //cara_sajian BARU
            $min_cara_sajian = 1;
            $mid_cara_sajian = 50;
            $max_cara_sajian = 100;
            $nilai_cara_sajian1 = round(($mid_cara_sajian - $request->cara_sajian) / ($mid_cara_sajian - $min_cara_sajian));
            $nilai_cara_sajian2 = round(($request->cara_sajian - $min_cara_sajian) / ($mid_cara_sajian - $min_cara_sajian));
            $nilai_cara_sajian3 = round(($request->cara_sajian - $mid_cara_sajian) / ($max_cara_sajian - $mid_cara_sajian));

             //cepat_sajian
            $min_cepat_sajian = 1;
            $mid_cepat_sajian = 50;
            $max_cepat_sajian = 100;
            $nilai_cepat_sajian1 = round(($mid_cepat_sajian - $request->cepat_sajian) / ($mid_cepat_sajian - $min_cepat_sajian));
            $nilai_cepat_sajian2 = round(($request->cepat_sajian - $min_cepat_sajian) / ($mid_cepat_sajian - $min_cepat_sajian));
            $nilai_cepat_sajian3 = round(($request->cepat_sajian - $mid_cepat_sajian) / ($max_cepat_sajian - $mid_cepat_sajian));


            $harga_terendah         = min($nilai_harga1,$nilai_harga2,$nilai_harga3);
            $cara_sajian_terendah   = min($nilai_cara_sajian1,$nilai_cara_sajian2,$nilai_cara_sajian3);
            $cepat_sajian_terendah  = min($nilai_cepat_sajian1,$nilai_cepat_sajian2,$nilai_cepat_sajian3);

            $Z1 = ((($request->harga*$harga_terendah)+($request->cara_sajian*$cara_sajian_terendah)+($request->cepat_sajian*$cepat_sajian_terendah)));
            $Z2 = (($harga_terendah+$cara_sajian_terendah+$cepat_sajian_terendah));
            $nilai = ($Z1/$Z2);
        }

        return view('metode',compact('nilai','order'));
    }
}
