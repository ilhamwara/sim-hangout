<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\WarungDataTable;
use App\Warung;

class WarungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WarungDataTable $dataTable)
    {
        return $dataTable->render('warung.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('warung.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Warung::where('nama',$request->nama)->count() > 0) {
            return redirect()->back()->with('alert-warning','Nama Warung sudah digunakan silahkan coba yang lain');
        }

        $warung = new Warung;
        $warung->nama         = $request->nama;
        $warung->save();

        return redirect('warung')->with('alert-success','Berhasil update data');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warung = Warung::findOrFail($id);
        return view('warung.update',compact('warung'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Warung::where('id','!=',$id)->where('nama',$request->nama)->count() > 0) {
            return redirect()->back()->with('alert-warning','Nama warung sudah digunakan silahkan coba yang lain');
        }

        $warung = Warung::findOrFail($id);
        $warung->nama    = $request->nama;
        $warung->save();

        return redirect('warung')->with('alert-success','Berhasil update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id)
    {
        Warung::findOrFail($id)->delete();
        return redirect('warung')->with('alert-success','Success deleted data');
    }
}
