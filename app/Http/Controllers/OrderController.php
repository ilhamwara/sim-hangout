<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\OrderDataTable;
use App\Order;
use App\Menu;
use App\Meja;

class OrderController extends Controller
{
    public function index(OrderDataTable $dataTable)
    {
        return $dataTable->render('order.index');
    }
    public function create()
    {
        $meja = Meja::all();
        $menu = Menu::all();
        $order = Order::groupBy('nama')->get();

        return view('order.create',compact('menu','meja','order'));
    }
    public function post_create(Request $request)
    {
        $menu = Menu::findOrFail($request->menu_id);
        $order = new Order;
        $order->nama    = $request->nama;
        $order->meja_id = $request->meja_id;
        $order->menu_id = $request->menu_id;
        $order->qty     = $request->qty;
        $order->harga   = $request->qty*$menu->harga;
        $order->tanggal = date('Y-m-d');
        $order->save();

        return redirect()->back()->with('alert-success','Berhasil update data');
    }
    public function delete($id)
    {
        Order::findOrFail($id)->delete();
        return redirect()->back()->with('alert-success','Berhasil update data');
    }
    public function detail($id)
    {
        $order = Order::findOrFail($id);
        $pesanan = Order::with('menu')->where('meja_id',$order->meja_id)->where('nama',$order->nama)->get();
        return view('order.detail',compact('order','pesanan'));
    }
    public function approve($id)
    {
        $order = Order::findOrFail($id);
        $pesanan = Order::where('meja_id',$order->meja_id)->where('nama',$order->nama)->update(['status' => 1]);

        return redirect()->back()->with('alert-success','Pesanan berhasil di konfirmasi');
    }
}
