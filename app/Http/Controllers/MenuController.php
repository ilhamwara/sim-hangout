<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\MenuDataTable;
use App\Menu;
use App\Warung;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MenuDataTable $dataTable)
    {
        return $dataTable->render('menu.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_menu($id)
    {
        $warung = Warung::findOrFail($id);
        return view('menu.create',compact('warung'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Menu::where('nama',$request->nama)->where('warung_id',$request->warung_id)->count() > 0) {
            return redirect()->back()->with('alert-warning','Nama menu sudah digunakan silahkan coba yang lain');
        }

        if ($request->hasFile('gambar')) {
            $path = public_path().'/menu/';
            $img = time().'.'.$request->file('gambar')->getClientOriginalExtension();
            $request->file('gambar')->move(realpath($path), $img);

            $menu = new Menu;
            $menu->warung_id    = $request->warung_id;
            $menu->nama         = $request->nama;
            $menu->harga        = $request->harga;
            $menu->gambar       = $img;
            $menu->save();
            
        }

        return redirect('menu/'.$request->warung_id)->with('alert-success','Berhasil update data');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MenuDataTable $dataTable,$id)
    {
        $warung = Warung::findOrFail($id);
        return $dataTable->with(['warung_id' => $id])->render('menu.index',compact('warung'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Menu::findOrFail($id);
        return view('menu.update',compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::findOrFail($id);
        
        if (Menu::where('id','!=',$id)->where('warung_id','!=',$menu->warung_id)->where('nama',$request->nama)->count() > 0) {
            return redirect()->back()->with('alert-warning','Nama menu sudah digunakan silahkan coba yang lain');
        }
        $menu->nama         = $request->nama;
        $menu->harga        = $request->harga;
        if ($request->hasFile('gambar')) {
            $path = public_path().'/menu/';
            $img = time().'.'.$request->file('gambar')->getClientOriginalExtension();
            $request->file('gambar')->move(realpath($path), $img);
            $menu->gambar       = $img;
        }
        $menu->save();
        return redirect('menu/'.$menu->warung_id)->with('alert-success','Berhasil update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id,$warung_id)
    {
        Menu::findOrFail($id)->delete();
        return redirect('menu/'.$warung_id)->with('alert-success','Success deleted data');
    }
}
