<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\MejaDataTable;
use App\Meja;

class MejaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MejaDataTable $dataTable)
    {
        return $dataTable->render('meja.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('meja.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Meja::where('nama',$request->nama)->count() > 0) {
            return redirect()->back()->with('alert-warning','Nama meja sudah digunakan silahkan coba yang lain');
        }

        $meja = new Meja;
        $meja->nama         = $request->nama;
        $meja->save();

        return redirect('meja')->with('alert-success','Berhasil update data');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meja = Meja::findOrFail($id);
        return view('meja.update',compact('meja'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Meja::where('id','!=',$id)->where('nama',$request->nama)->count() > 0) {
            return redirect()->back()->with('alert-warning','Nama meja sudah digunakan silahkan coba yang lain');
        }

        $meja = Meja::findOrFail($id);
        $meja->nama    = $request->nama;
        $meja->save();

        return redirect('meja')->with('alert-success','Berhasil update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete($id)
    {
        Meja::findOrFail($id)->delete();
        return redirect('meja')->with('alert-success','Success deleted data');
    }
}
