<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    public function warung()
    {
    	return $this->belongsTo('\App\Warung','warung_id','id');
    }
}
