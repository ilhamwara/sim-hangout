<?php

namespace App\DataTables;

use App\Meja;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use URL;

class MejaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('action',function($query){
              return '<a class="btn btn-block btn-info" href="'.URL::to("meja/".$query->id."/edit").'"><i class="fa fa-eye"></i> Edit</a> <a class="btn btn-block btn-danger remove_data" href="#"" data-href="'.URL::to("meja/delete/".$query->id).'"><i class="fa fa-trash"></i> Delete</a>';
            })->addColumn('action', 'meja.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Meja $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Meja $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('meja-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    // ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        // Button::make('export'),
                        // Button::make('print'),
                        // Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('No')->render(function() {
              return 'function(data,type,fullData,meta){
                  return meta.settings._iDisplayStart+meta.row+1;}';
          })->width(10),
          Column::make('nama')->title('Nama Meja'),
          Column::computed('action')->title('Actions')->width(10),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Meja_' . date('YmdHis');
    }
}
