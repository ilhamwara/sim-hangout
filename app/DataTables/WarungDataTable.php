<?php

namespace App\DataTables;

use App\Warung;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use URL;

class WarungDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('action',function($query){
              return '<a class="btn btn-block btn-info" href="'.URL::to("warung/".$query->id."/edit").'"><i class="fa fa-eye"></i> Edit</a> <a class="btn btn-block btn-success" href="'.URL::to("menu/".$query->id).'"><i class="fa fa-list"></i> Menu</a> <a class="btn btn-block btn-danger remove_data" href="#"" data-href="'.URL::to("warung/delete/".$query->id).'"><i class="fa fa-trash"></i> Delete</a>';
            })->editColumn('menu',function($query){
              return $query->getMenu();
            })->addColumn('action', 'warung.action')->rawColumns(['menu','action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Warung $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Warung $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('warung-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    // ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        // Button::make('export'),
                        // Button::make('print'),
                        // Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('No')->render(function() {
              return 'function(data,type,fullData,meta){
                  return meta.settings._iDisplayStart+meta.row+1;}';
          })->width(10),
          Column::make('nama')->title('Nama Warung')->width(100),
          Column::computed('menu')->title('Menu'),
          Column::computed('action')->title('Actions')->width(150),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Warung_' . date('YmdHis');
    }
}
