<?php

namespace App\DataTables;

use App\Order;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use URL;
use Auth;
use DB;

class ReportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('action',function($query){
              return '<a class="btn btn-block btn-primary" href="'.URL::to("order/detail/".$query->id).'"><i class="fa fa-eye"></i> Detail Order</a>';
            })->editColumn('status',function($query){
                if ($query->status == 0) {
                    return '<span class="label label-danger">Belum Bayar</span>';
                }else{
                    return '<span class="label label-success">Sudah Bayar</span>';
                }
            })->editColumn('total',function($query){
                return 'Rp '.str_replace(',','.',number_format($query->total));
            })->rawColumns(['action','status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        if (Auth::user()->type == 'warung') {
            return $model->groupBy(['order.nama','meja_id'])->join('menu','order.menu_id','=','menu.id')
            ->join('meja','order.meja_id','=','meja.id')
            ->join('warung','menu.warung_id','=','warung.id')
            ->where('menu.warung_id',Auth::user()->warung_id)
            ->select('order.id',DB::raw('SUM(order.harga) as total'),'order.tanggal','order.nama','menu.nama as menu_nama','meja.nama as meja_nama','warung.nama as warung_nama','order.status')
            ->newQuery();
        }else{
            return $model->groupBy(['order.nama','meja_id'])->join('menu','order.menu_id','=','menu.id')
            ->join('meja','order.meja_id','=','meja.id')
            ->join('warung','menu.warung_id','=','warung.id')
            ->select('order.id',DB::raw('SUM(order.harga) as total'),'order.tanggal','order.nama','menu.nama as menu_nama','meja.nama as meja_nama','warung.nama as warung_nama','order.status')
            ->newQuery();
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('report-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    // ->orderBy(1)
                    ->buttons(
                        // Button::make('create'),
                        // Button::make('export'),
                        // Button::make('print'),
                        // Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')->title('Actions')->width(50),
            Column::make('id')->title('No')->render(function() {
                return 'function(data,type,fullData,meta){
                    return meta.settings._iDisplayStart+meta.row+1;}';
                })->width(10),
            Column::make('nama')->title('Nama Pesanan'),
            'Meja'      => ['data' => 'meja_nama', 'name' => 'meja.nama','width' => '50'],
            Column::computed('total')->title('Harga'),
            Column::computed('status')->title('Status'),
            Column::make('tanggal')->title('Tanggal Order'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Report_' . date('YmdHis');
    }
}
