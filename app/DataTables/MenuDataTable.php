<?php

namespace App\DataTables;

use App\Menu;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use URL;

class MenuDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('harga',function($query){
              return 'Rp '.number_format($query->harga);
            })->editColumn('action',function($query){
              return '<a class="btn btn-block btn-info" href="'.URL::to("menu/".$query->id."/edit").'"><i class="fa fa-eye"></i> Edit</a> <a class="btn btn-block btn-danger remove_data" href="#"" data-href="'.URL::to("menu/delete/".$query->id."/".$this->warung_id).'"><i class="fa fa-trash"></i> Delete</a>';
            })->editColumn('gambar',function($query){
              return '<img width="100" class="img-fluid" src="'.asset('menu/').'/'.$query->gambar.'">';
            })->rawColumns(['gambar','action'])->addColumn('action', 'menu.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Menu $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Menu $model)
    {
        if (empty($this->warung_id) && ($this->warung_id == NULL)) {
            return $model->with('warung')->newQuery();
        }else{
            return $model->where('warung_id',$this->warung_id)->with('warung')->newQuery();
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('menu-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    // ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        // Button::make('export'),
                        // Button::make('print'),
                        // Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('No')->render(function() {
                return 'function(data,type,fullData,meta){
                    return meta.settings._iDisplayStart+meta.row+1;}';
                })->width(10),
            Column::make('warung.nama')->title('Nama Warung'),
            Column::make('nama')->title('Nama Menu'),
            Column::make('harga')->title('Harga Menu'),
            Column::computed('gambar')->title('Gambar'),
            Column::computed('action')->title('Actions')->width(150),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Menu_' . date('YmdHis');
    }
}
