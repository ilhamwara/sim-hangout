<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warung extends Model
{
    protected $table = 'warung';

    public function getMenu() {

        $sdAll = Menu::where("warung_id", $this->id)->get();
        $sdCount = Menu::where("warung_id", $this->id)->count();

        $res = "Belum ada data";
        if($sdCount > 0){
            $res = [];
            foreach ($sdAll as $key => $value) {
                $d = $value->nama;
                array_push($res, $d);
            }
            $res = implode("<br />", $res);
        }
        return $res;
    }
}
